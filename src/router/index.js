import Vue from "vue";
import VueRouter from "vue-router";
import Kuechen from "../views/kueche/Kuechen.vue"
import About from "../views/About.vue"
import KArbeiter from "../views/kueche/KMitarbeiter.vue";
import KGeraete from "../views/kueche/KMitarbeiterGeraete.vue";
import KJobs from "../views/kueche/KMitarbeiterJobs.vue";
import Home from "../views/Home.vue";
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

import StadiumHome from "../views/stadium/Stadium.vue"
import Stadien from "../views/stadium/Stadium_ALL.vue"
import StadienMitarbeiter from "../views/stadium/Stadium_Mitarbeiter.vue"
import StadienUtensilien from "../views/stadium/Stadium_Utensilien.vue"
import StadienEntlassungen from "../views/stadium/Stadium_Entlassungen.vue"

Vue.use(VueMaterial);
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "About",
    component: About
  },
  {
    path: "/kuechen",
    name: "Küchen",
    component: Kuechen
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    //component: () =>
      //import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/kuechen/mitarbeiter",
    name: "Küchen Mitarbeiter",
    component: KArbeiter
  },
  {
    path: "/kuechen/geraete",
    name: "Küchen Geräte",
    component: KGeraete
  },
  {
    path: "/kuechen/mitarbeiter/jobs",
    name: "Küchen Jobs",
    component: KJobs
  },
  {
    path: "/stadium",
    name: "Stadium",
    component: StadiumHome
  },
  {
    path: "/stadien",
    name: "Stadium",
    component: Stadien
  },
  {
    path: "/stadien/mitarbeiter",
    name: "Stadium Mitarbeiter",
    component: StadienMitarbeiter
  },
  {
    path: "/stadien/utensilien",
    name: "Stadium Utensilien",
    component: StadienUtensilien
  },
  {
    path: "/stadien/mitarbeiter/entlassungen",
    name: "Stadium Entlassungen",
    component: StadienEntlassungen
  }
];

const router = new VueRouter({
  routes
});

export default router;
